
#ifndef __FREERTOS_TASK_LOG_H__
#define __FREERTOS_TASK_LOG_H__


#include "gd32f10x.h"

void freertos_task_log(void *args);


#endif /* __FREERTOS_TASK_LOG_H__ */
