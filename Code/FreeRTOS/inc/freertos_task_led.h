
#ifndef __FREEFTOS_TASK_LED_H__
#define __FREEFTOS_TASK_LED_H__

#include "gd32f10x.h"

void freertos_task_led(void *args);

#endif /* __FREEFTOS_TASK_LED_H__ */
