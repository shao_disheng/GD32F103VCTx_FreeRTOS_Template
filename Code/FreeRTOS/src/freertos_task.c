

#include "FreeRTOS.h"
#include "task.h"

#include "freertos_task_led.h"
#include "freertos_task_log.h"

#include "freertos_task.h"

TaskHandle_t freertos_task_led_handle;
TaskHandle_t freertos_task_log_handle;

void freertos_task_init(void)
{
	xTaskCreate(freertos_task_led, "task_led", 64, NULL, 5, &freertos_task_led_handle);
	xTaskCreate(freertos_task_log, "task_log", 64, NULL, 5, &freertos_task_log_handle);
	
	vTaskStartScheduler();
}





