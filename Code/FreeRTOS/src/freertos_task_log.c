
#include "gd32f10x_rcu.h"
#include "gd32f10x_gpio.h"

#include "FreeRTOS.h"
#include "task.h"

#include "generic_log.h"

#include "freertos_task_log.h"

void freertos_task_log(void *args)
{
	/* 初始化 LED */
	rcu_periph_clock_enable(RCU_GPIOE);
	gpio_init(GPIOE, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_2);
	
	for(;;) {
		LOG("Task LOG Running");
		vTaskDelay(1000);
	}
}
