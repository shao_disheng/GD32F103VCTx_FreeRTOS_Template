
#include "gd32f10x_rcu.h"
#include "gd32f10x_gpio.h"

#include "FreeRTOS.h"
#include "task.h"

#include "freertos_task_led.h"


void freertos_task_led(void *args)
{
	/* 初始化 LED */
	rcu_periph_clock_enable(RCU_GPIOE);
	gpio_init(GPIOE, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_2);
	
	for(;;) {
		gpio_bit_set(GPIOE, GPIO_PIN_2);
		vTaskDelay(500);
		gpio_bit_reset(GPIOE, GPIO_PIN_2);
		vTaskDelay(500);
	}
}

