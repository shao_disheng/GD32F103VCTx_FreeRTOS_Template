

#ifndef __GLOBAL_LOG_H__
#define __GLOBAL_LOG_H__

#include <stdio.h>
#include "gd32f10x.h"

#define 	LOG_DBG_TRIGGER		(1U)

#define LOG(fmt, ...)                                                                                                  \
    (void)({                                                                                                           \
        printf("\t" fmt, ##__VA_ARGS__);                                                                               \
        printf("\r\n");                                                                                                \
    })

#define LOG_ERROR(fmt, ...)                                                                                            \
    (void)({                                                                                                           \
        printf("\t-----------------------------------------------------\r\n");                                         \
        printf("\tError\t" fmt, ##__VA_ARGS__);                                                                        \
        printf("\t-----------------------------------------------------\r\n");                                         \
    })

#define LOG_LINE_LIGHT (void)({ printf("\t-----------------------------------------------------\r\n"); })

#define LOG_LINE_MIDDLE (void)({ printf("\t=====================================================\r\n"); })

#define LOG_LINE_HEAVY (void)({ printf("\t#####################################################\r\n"); })

#if (LOG_DBG_TRIGGER)

#define LOG_DBG(fmt, ...)                                                                                              \
    (void)({                                                                                                           \
        printf("\t" fmt, ##__VA_ARGS__);                                                                               \
        printf("\r\n");                                                                                                \
    })

#define LOG_DBG_ERROR(fmt, ...)                                                                                        \
    (void)({                                                                                                           \
        printf("\t-----------------------------------------------------\r\n");                                         \
        printf("\tError\t" fmt, ##__VA_ARGS__);                                                                        \
		printf("\r\n");																								   \
        printf("\t-----------------------------------------------------\r\n");                                         \
    })

#define LOG_DBG_LINE_LIGHT                                                                                             \
        (void)({ \
                printf("\t-----------------------------------------------------\r\n"); \
                )}

#define LOG_DBG_LINE_MIDDLE (void)({ printf("\t=====================================================\r\n"); })
#define LOG_DBG_LINE_HEAVY (void)({ printf("\t#####################################################\r\n"); })
#else

#define LOG_DBG(fmt, ...) (void)({})
#define LOG_DBG_ERROR(fmt, ...) (void)({})
#define LOG_DBG_LINE_LIGHT (void)({})
#define LOG_DBG_LINE_MIDDLE (void)({})
#define LOG_DBG_LINE_HEAVY (void)({})
#endif




#endif /* __GLOBAL_LOG_H__ */
