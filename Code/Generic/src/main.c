
/**
 *******************************************************************************
 *	@file	main.c
 *	@brief	程序逻辑功能入口
 *	@date	2023-06-30 created by ZK30625
 *******************************************************************************
 */
 
/* Generic Support ************************************************************/
#include "generic.h"

#include "freertos_task.h"

/** 
 *	@brief	主函数
 *
 *	@scope	Global
 *
 * 	@param	None
 * 
 *	@retval	None
 */
int main(void)
{	 
	systick_config();
	bsp_init();
	
	freertos_task_init();
	
	while(1);
}

