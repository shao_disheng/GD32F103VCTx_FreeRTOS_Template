
/**
 *******************************************************************************
 *	@file	generic_usart.c
 *	@brief	串口初始化
 *	@date	2023-06-30 created by ZK30625
 *******************************************************************************
 */
 
/* Standard Libraries Support *************************************************/
#include <stdio.h>

/* Firmware Libraries Support *************************************************/
#include "gd32f10x.h"
#include "gd32f10x_rcu.h"
#include "gd32f10x_gpio.h"
#include "gd32f10x_usart.h"
#include "gd32f10x_dma.h"

/* Generic Support ************************************************************/
#include "generic_usart.h"


/* Local Defination ***********************************************************/
/* 板卡的串口外设 */
#define GENERIC_USART 		(USART0)
#define GENERIC_BAUDRATE 	(115200U)

#define USART0_RDATA_ADDRESS(USARTx)      ((uint32_t)&USART_DATA(USARTx))

__IO uint8_t rxbuffer[256];
__IO uint8_t rx_count = 0;
__IO uint8_t receive_flag = 0;


void generic_usart_init(void)
{
	/* 使能串口中断 */
	nvic_irq_enable(USART0_IRQn, 0, 0);
	
	/* DMA 初始化 */
	dma_parameter_struct dma_init_struct;
	rcu_periph_clock_enable(RCU_DMA0);
	
	// 默认 DMA 0 的通道 4 是串口 0 的 接收通道
	dma_deinit(DMA0, DMA_CH4);
	dma_init_struct.direction = DMA_PERIPHERAL_TO_MEMORY;
	dma_init_struct.memory_addr = (uint32_t)rxbuffer;
	dma_init_struct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;
	dma_init_struct.memory_width = DMA_MEMORY_WIDTH_8BIT;
	dma_init_struct.number = 256;
	dma_init_struct.periph_addr = USART0_RDATA_ADDRESS(USART0);
	dma_init_struct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;
	dma_init_struct.periph_width = DMA_PERIPHERAL_WIDTH_8BIT;
	dma_init_struct.priority = DMA_PRIORITY_ULTRA_HIGH;
	dma_init(DMA0, DMA_CH4, &dma_init_struct);
	/* configure DMA mode */
	dma_circulation_disable(DMA0, DMA_CH4);
	/* enable DMA channel4 */
	dma_channel_enable(DMA0, DMA_CH4);
	
	/* USART 初始化 */
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_USART0);

	// GPIO 初始化
    gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9);
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_10);

    /* USART 初始化 */
    usart_deinit(GENERIC_USART);
    usart_baudrate_set(GENERIC_USART, GENERIC_BAUDRATE);
    usart_receive_config(GENERIC_USART, USART_RECEIVE_ENABLE);
    usart_transmit_config(GENERIC_USART, USART_TRANSMIT_ENABLE);
    usart_dma_receive_config(GENERIC_USART, USART_RECEIVE_DMA_ENABLE);
    usart_dma_transmit_config(GENERIC_USART, USART_TRANSMIT_DMA_ENABLE);
    usart_enable(GENERIC_USART);

	// 使能串口 IDLE 中断
	usart_interrupt_enable(USART0, USART_INT_IDLE);
}

/************************* System Call *****************************/

/* retarget the C library printf function to the USART */ 
int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));
    return ch;
}


